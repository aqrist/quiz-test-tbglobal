<?php
// Map huruf
$digitToLetter = [
    1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H',
    9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O',
    16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V',
    23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z'
];

// Digit yg disediakan
$digits = "1243752521494312";

// Define fungsi rekursif untuk generate kombinasi
function generateCombinations($index, $current, $digits, $result, $digitToLetter) {
    if ($index >= strlen($digits)) {
        $result[] = $current;
        return $result;
    }

    $result = generateCombinations($index + 1, $current . $digitToLetter[$digits[$index]], $digits, $result, $digitToLetter);
    if ($index < strlen($digits) - 1 && isset($digitToLetter[$digits[$index] . $digits[$index + 1]])) {
        $result = generateCombinations($index + 2, $current . $digitToLetter[$digits[$index] . $digits[$index + 1]], $digits, $result, $digitToLetter);
    }
    return $result;
}

// Generate semua kombinasi
$combinations = generateCombinations(0, '', $digits, [], $digitToLetter);

echo "total kombinasinya adalah ". count($combinations) ." \n";
foreach ($combinations as $combination) {
    echo "$combination\n";
}
?>
