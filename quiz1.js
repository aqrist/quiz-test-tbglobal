function findPatterns(input, pattern) {
    const patternLength = pattern.length;
    const patterns = [];

    for (let i = 0; i <= input.length - patternLength; i++) {
        const substring = input.substring(i, i + patternLength);
        const sortedSubstring = substring.split('').sort().join('');
        const sortedPattern = pattern.split('').sort().join('');

        if (sortedSubstring === sortedPattern) {
            patterns.push(substring);
        }
    }

    return patterns;
}

const input = "ABDCKDHJABDCBDAUOQJDBADCLDLCHBCBABCBAABCDAJDBABDCABDABDBCADBCASSJGABCDAUTACBDBQWUDNCDBCADKDHABDJGBDABCBDBADCACADBADBCBAD";
const pattern = "ABCD";
const patterns = findPatterns(input, pattern);

console.log(patterns);
console.log('Pola yang ditemukan:', patterns.length);
